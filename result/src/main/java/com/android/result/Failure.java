package com.android.result;

public class Failure<T> extends Result<T> {
    private final Throwable value;

    public Failure(Throwable value) {
        this.value = value;
    }

    @Override
    public Boolean isSuccess() {
        return false;
    }

    @Override
    public Boolean isFailure() {
        return true;
    }

    @Override
    public T getOrDefault(T defValue) {
        return defValue;
    }

    @Override
    public Throwable getThrowableOrDefault(Throwable defValue) {
        return value;
    }

    @Override
    public void onSuccess(Consumer<T> func) { }

    @Override
    public void onFailure(Consumer<Throwable> func) {
        func.accept(value);
    }

    @Override
    public <V> Result<V> map(Function<T, V> func) {
        return new Failure<>(value);
    }

    @Override
    public <V> Result<V> flatMap(Function<T, Result<V>> func) {
        return new Failure<>(value);
    }

    @Override
    public Result<T> recover(Function<Throwable, T> func) {
        try {
            return new Success<>(func.apply(value));
        } catch (Exception e) {
            return new Failure<>(e);
        }
    }

    @Override
    public String toString() {
        return "Failure{" +
                "value=" + value +
                '}';
    }
}
