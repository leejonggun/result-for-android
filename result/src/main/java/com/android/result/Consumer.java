package com.android.result;

public interface Consumer<T> {
    void accept(T value);
}
