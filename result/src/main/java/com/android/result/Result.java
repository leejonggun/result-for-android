package com.android.result;

public abstract class Result<T> {
    abstract public Boolean isSuccess();

    abstract public Boolean isFailure();

    abstract public T getOrDefault(T defValue);
    abstract public Throwable getThrowableOrDefault(Throwable defValue);

    abstract public void onSuccess(Consumer<T> func);
    abstract public void onFailure(Consumer<Throwable> func);

    abstract public <V> Result<V> map(Function<T, V> func);
    abstract public <V> Result<V> flatMap(Function<T, Result<V>> func);
    abstract public Result<T> recover(Function<Throwable, T> func);

    public static <V> Result<V> of(Supplier<V> func) {
        try {
            return new Success<>(func.supply());
        } catch (Exception e) {
            return new Failure<>(e);
        }
    }
}
