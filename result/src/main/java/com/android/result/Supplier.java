package com.android.result;

public interface Supplier<T> {
    T supply();
}
