package com.android.result;

public class Success<T> extends Result<T> {
    private final T value;

    public Success(T value) {
        this.value = value;
    }

    @Override
    public Boolean isSuccess() {
        return true;
    }

    @Override
    public Boolean isFailure() {
        return false;
    }

    @Override
    public T getOrDefault(T defValue) {
        return value;
    }

    @Override
    public Throwable getThrowableOrDefault(Throwable defValue) {
        return defValue;
    }

    @Override
    public void onSuccess(Consumer<T> func) {
        func.accept(value);
    }

    @Override
    public void onFailure(Consumer<Throwable> func) { }

    @Override
    public <V> Result<V> map(Function<T, V> func) {
        try {
            return new Success<>(func.apply(value));
        } catch (Exception e) {
            return new Failure<>(e);
        }
    }

    @Override
    public <V> Result<V> flatMap(Function<T, Result<V>> func) {
        try {
            return func.apply(value);
        } catch (Exception e) {
            return new Failure<>(e);
        }
    }

    @Override
    public Result<T> recover(Function<Throwable, T> func) {
        return this;
    }

    @Override
    public String toString() {
        return "Success{" +
                "value=" + value +
                '}';
    }
}
