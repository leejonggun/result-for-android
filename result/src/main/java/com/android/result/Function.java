package com.android.result;

public interface Function<T, S> {
    S apply(T value);
}
