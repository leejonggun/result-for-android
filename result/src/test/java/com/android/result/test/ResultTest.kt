package com.android.result.test

import com.android.result.Result
import com.android.result.Failure
import com.android.result.Success
import org.junit.Test
import java.util.function.Supplier

class ResultTest {
    private val success = "success"
    private val failure = Exception("failure")

    private val mapped_string = "mapped_string"
    private val mapped_int = 99999

    private val default_string = "default value"
    private val default_int = 55555
    private val default_exception = Exception("default exception")

    @Test
    fun Create_Success() {
        val ret: Result<String> = Success<String>(success)
        validateResult(ret)
        assert(ret.getOrDefault(default_string) == success)
    }

    @Test
    fun Create_Failure() {
        val ret: Result<String> = Failure<String>(failure)
        validateResult(ret)
        assert(ret.getOrDefault(default_string) == default_string)
    }

    @Test
    fun Return_Success() {
        val ret: Result<String> = Result.of { success }
        validateResult(ret)
        assert(ret.getOrDefault(default_string) == success)
    }

    @Test
    fun Return_Success_Throwable() {
        val ret: Result<Exception> = Result.of { failure }
        validateResult(ret)
        assert(ret.getOrDefault(default_exception) == failure)
    }

    @Test
    fun Return_Failure() {
        val ret: Result<String> = Result.of { throw failure }
        validateResult(ret)
        assert(ret.getOrDefault(default_string) == default_string)
    }

    @Test
    fun Success_Eval_OnSuccess() {
        val ret: Result<String> = Result.of { success }
        validateResult(ret)
        val after_success = ret.onSuccess { assert(it == success) }
        assert(after_success is Unit)

        val on_success_return_something = ret.onSuccess { assert(it == success) }
        assert(on_success_return_something is Unit)
    }

    @Test
    fun Success_Dont_Eval_OnFailure() {
        val ret: Result<String> = Result.of { success }
        validateResult(ret)
        val after_failure = ret.onFailure { assert(false) {"Success don't evaludate onFailure"} }
        assert(after_failure is Unit)
    }

    @Test
    fun Success_Eval_Map() {
        val ret: Result<String> = Result.of { success }
        validateResult(ret)
        val map_string = ret.map {
            assert(it == success)
            mapped_string
        }
        validateResult(map_string)
        assert(map_string.getOrDefault(default_string) == mapped_string)

        val map_int = map_string.map {
            assert(it == mapped_string)
            mapped_int
        }
        validateResult(map_int)
        assert(map_int.getOrDefault(default_int) == mapped_int)
    }

    @Test
    fun Success_Eval_FlatMap() {
        val ret: Result<String> = Result.of { success }
        validateResult(ret)
        val flatmap_string = ret.flatMap {
            assert(it == success)
            Success(mapped_string)
        }
        validateResult(flatmap_string)
        assert(flatmap_string.getOrDefault(default_string) == mapped_string)

        val flatmap_int = flatmap_string.flatMap {
            assert(it == mapped_string)
            Success(mapped_int)
        }
        validateResult(flatmap_int)
        assert(flatmap_int.getOrDefault(default_int) == mapped_int)

        val flatmap_failure = ret.flatMap {
            assert(it == success)
            Failure<Int>(failure)
        }
        validateResult(flatmap_failure)
        assert(flatmap_failure.getOrDefault(default_int) == default_int)
    }

    @Test
    fun Success_Dont_Eval_Recover() {
        val ret: Result<String> = Result.of { success }
        validateResult(ret)
        val recover_string = ret.recover {
            assert(false) { "Don't eval this code block" }
            default_string
        }
        validateResult(recover_string)
    }

    private fun <T: Any> validateResult(value: Result<T>) {
        when (value) {
            is Success<T> -> {
                assert(value is Result)
                assert(value is Success)
                assert(value !is Failure)
                assert(value.isSuccess == true)
                assert(value.isFailure == false)
                assert(value.getThrowableOrDefault(default_exception) == default_exception)
            }
            is Failure<T> -> {
                assert(value is Result)
                assert(value is Failure)
                assert(value !is Success)
                assert(value.isFailure == true)
                assert(value.isSuccess == false)
                assert(value.getThrowableOrDefault(default_exception) == failure)
            }
            else -> {
                assert(false) { "Invalidate Result type $value" }
            }
        }
    }

    @Test
    fun Failure_Dont_Eval_OnSuccess() {
        val ret: Result<String> = Result.of { throw failure }
        validateResult(ret)
        val after_success = ret.onSuccess { assert(false) { "Don't eval this code block" } }
        assert(after_success is Unit)
    }

    @Test
    fun Failure_Eval_OnFailure() {
        val ret: Result<String> = Result.of { throw failure }
        validateResult(ret)
        val after_failure = ret.onFailure {
            assert(it == failure)
        }
        assert(after_failure is Unit)
    }

    @Test
    fun Failure_Dont_Eval_Map() {
        val ret: Result<String> = Result.of { throw failure }
        validateResult(ret)
        val map_string = ret.map {
            assert(false) { "Don't eval this code block" }
            mapped_string
        }
        validateResult(map_string)
        assert(map_string.getOrDefault(default_string) == default_string)
    }

    @Test
    fun Failure_Dont_Eval_FlatMap() {
        val ret: Result<String> = Result.of { throw failure }
        validateResult(ret)
        val flatmap_string = ret.flatMap {
            assert(false) { "Don't eval this code block" }
            Success(mapped_string)
        }
        validateResult(flatmap_string)
        assert(flatmap_string.getOrDefault(default_string) == default_string)

        val flatmap_int = flatmap_string.flatMap {
            assert(false) { "Don't eval this code block" }
            Success(mapped_int)
        }
        validateResult(flatmap_int)
        assert(flatmap_int.getOrDefault(default_int) == default_int)

        val flatmap_failure = ret.flatMap {
            assert(it == success)
            Failure<Int>(failure)
        }
        validateResult(flatmap_failure)
        assert(flatmap_failure.getOrDefault(default_int) == default_int)
    }

    @Test
    fun Failure_Eval_Recover() {
        val ret: Result<String> = Result.of { throw failure }
        validateResult(ret)
        val recover_string = ret.recover {
            success
        }
        validateResult(recover_string)
        assert(recover_string.getOrDefault(default_string) == success)
    }
}